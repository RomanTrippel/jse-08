package ru.trippel.tm.entity;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import java.util.UUID;

@NoArgsConstructor
public abstract class AbstractEntity     {

    @Getter
    @NotNull
    protected final String id = UUID.randomUUID().toString();

}

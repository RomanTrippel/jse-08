package ru.trippel.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.repository.ITaskRepository;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;

import java.util.*;

@NoArgsConstructor
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @NotNull
    public List<Task> findAll(@NotNull final String userId) {
        @NotNull final List<Task> listTemp = new LinkedList<>();
        @NotNull final List<Task> listAll  = new LinkedList<>(map.values());
        for (@NotNull final Task task: listAll) {
            if (task.getUserId().equals(userId)) listTemp.add(task);
        }
        return listTemp;
    }

    @Override
    public void clear(@NotNull final String userId){
        @NotNull final List<Task> listAll  = new LinkedList<>(map.values());
        for (@NotNull final Task task: listAll) {
            if (task.getUserId().equals(userId)) map.remove(task.getId());
        }
    }

    @Override
    public void removeByProjectId(@NotNull final String projectId) {
        @NotNull final List<Task> listAll  = new LinkedList<>(map.values());
        for (@NotNull final Task task: listAll) {
            if (task.getProjectId().equals(projectId)) map.remove(task.getId());
        }
    }

}
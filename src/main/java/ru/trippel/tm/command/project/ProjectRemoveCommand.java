package ru.trippel.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.service.ProjectService;
import ru.trippel.tm.service.TerminalService;
import java.util.List;

public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "PROJECT_REMOVE";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove a project.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User currentUser = state.getCurrentUser();
        @NotNull final String userId = currentUser.getId();
        @Nullable final List<Project> projectList = serviceLocator.getProjectService().findAll(userId);
        if (projectList == null) return;
        int projectNum = -1;
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println(i+1 + ". " + projectList.get(i).getName());
        }
        System.out.println("Enter a project number to Delete:");
        projectNum +=  Integer.parseInt(serviceLocator.getTerminalService().read());
        @NotNull final String projectId = projectList.get(projectNum).getId();
        serviceLocator.getProjectService().remove(projectId);
        serviceLocator.getTaskService().removeAllByProjectId(projectId);
        System.out.println("The project has been deleted.");
    }

}

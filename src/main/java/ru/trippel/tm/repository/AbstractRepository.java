package ru.trippel.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.IRepository;
import ru.trippel.tm.entity.AbstractEntity;
import ru.trippel.tm.entity.Project;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
public abstract class AbstractRepository <T extends AbstractEntity> implements IRepository<T> {

    @NotNull
    protected Map<String, T> map = new LinkedHashMap<>();

    @NotNull
    @Override
    public List<T> findAll() {
        return new LinkedList<T>(map.values());
    }

    @Nullable
    @Override
    public T findOne(@NotNull final String id) {
        return map.get(id);
    }

    @Nullable
    @Override
    public T persist(@NotNull final T t) {
        return map.put(t.getId(),t);
    }

    @Nullable
    @Override
    public T merge(@NotNull final T t) {
        return map.merge(t.getId(), t, (entityOld, entityNew) -> entityNew);
    }

    @Nullable
    @Override
    public T remove(@NotNull final String id) {
        return map.remove(id);
    }

    @Override
    public void removeAll() {
        map.clear();
    }

}

package ru.trippel.tm.util;

import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.trippel.tm.command.AbstractCommand;
import java.util.Set;

public class PackageClassFinderUtil {

    @NotNull
    public static Class<? extends AbstractCommand>[] getAllClasses(@NotNull final String pathPackage) {
        @NotNull final Reflections reflections = new Reflections(pathPackage);
        @NotNull final Set<Class<? extends AbstractCommand>> allClasses =
                reflections.getSubTypesOf(AbstractCommand.class);
        @NotNull final int size = allClasses.size();
        @NotNull Class<? extends AbstractCommand>[] resultArray = new Class[size];
        allClasses.toArray(resultArray);
        return allClasses.toArray(resultArray);
    }

}
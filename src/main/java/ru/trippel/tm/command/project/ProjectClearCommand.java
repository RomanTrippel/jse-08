package ru.trippel.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.context.IState;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.service.ProjectService;

public final class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "PROJECT_CLEAR";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        @Nullable final User currentUser = state.getCurrentUser();
        @NotNull final String userId = currentUser.getId();
        serviceLocator.getProjectService().clear(userId);
        System.out.println("The list of projects has been cleared.");
    }

}

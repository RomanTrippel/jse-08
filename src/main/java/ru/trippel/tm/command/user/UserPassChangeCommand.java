package ru.trippel.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.util.HashPasswordUtil;
import ru.trippel.tm.service.TerminalService;

@NoArgsConstructor
public final class UserPassChangeCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "USER_PASSCHANGE";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change Password.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User currentUser = state.getCurrentUser();
        @NotNull final String userId = currentUser.getId();
        @Nullable final User user = serviceLocator.getUserService().findOne(userId);
        if (user == null) return;
        System.out.println("Enter new Password");
        @NotNull final String newPassword = serviceLocator.getTerminalService().read();
        @NotNull final String newPasswordHash = HashPasswordUtil.getHash(newPassword);
        user.setPassword(newPasswordHash);
        state.setCurrentUser(new User());
        System.out.println("Changes applied. Login required again.");
    }

}

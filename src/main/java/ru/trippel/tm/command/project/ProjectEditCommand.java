package ru.trippel.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.util.DateUtil;

import java.util.Date;
import java.util.List;

public final class ProjectEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "PROJECT_EDIT";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit a project.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User currentUser = state.getCurrentUser();
        @NotNull final String userId = currentUser.getId();
        @Nullable final List<Project> projectList = serviceLocator.getProjectService().findAll(userId);
        if (projectList == null) return;
        int projectNum = -1;
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println(i+1 + ". " + projectList.get(i).getName());
        }
        System.out.println("Enter a project number to Edit.");
        projectNum +=  Integer.parseInt(serviceLocator.getTerminalService().read());
        @NotNull final String projectId = projectList.get(projectNum).getId();
        System.out.println("Enter new Name.");
        @NotNull final String newName = serviceLocator.getTerminalService().read();
        @Nullable final Project project = serviceLocator.getProjectService().findOne(projectId);
        if (project == null) return;
        project.setName(newName);
        System.out.println("Enter description.");
        @NotNull final String newDescription = serviceLocator.getTerminalService().read();
        project.setDescription(newDescription);
        System.out.println("Enter the finish date of the project.(For example, 01-01-2020)");
        @NotNull final String newDateFinish = serviceLocator.getTerminalService().read();
        @NotNull Date date = DateUtil.parseDate(newDateFinish);
        project.setDateFinish(date);
        serviceLocator.getProjectService().merge(project);
        System.out.println("Changes applied.");
    }

}

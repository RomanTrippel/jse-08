package ru.trippel.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.service.TaskService;

@NoArgsConstructor
public final class TaskClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "TASK_CLEAR";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() {
        @Nullable final User currentUser = state.getCurrentUser();
        @NotNull final String userId = currentUser.getId();
        serviceLocator.getTaskService().clear(userId);
        System.out.println("The list of tasks has been cleared.");
    }

}

package ru.trippel.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.IRepository;
import ru.trippel.tm.api.repository.ITaskRepository;
import ru.trippel.tm.api.service.ITaskService;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.repository.AbstractRepository;

import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    public TaskService(@NotNull final ITaskRepository repository) {
        super(repository);
        taskRepository = repository;
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null) return null;
        if (userId.isEmpty()) return null;
        return repository.findAll(userId);
    }

    @Override
    public void removeAllByProjectId(@NotNull String projectId) {
        if (projectId.isEmpty()) return;
        taskRepository.removeByProjectId(projectId);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null) return;
        if (userId.isEmpty()) return;
        taskRepository.clear(userId);
    }

}

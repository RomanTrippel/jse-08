package ru.trippel.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.service.TerminalService;

public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "PROJECT_CREATE";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create a new project.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Project project = new Project();
        @Nullable final User currentUser = state.getCurrentUser();
        @NotNull final String userId = currentUser.getId();
        System.out.println("Enter project name.");
        @NotNull final String name = serviceLocator.getTerminalService().read();
        project.setName(name);
        project.setUserId(userId);
        serviceLocator.getProjectService().persist(project);
        System.out.println("The project \"" + project.getName() + "\" added!");
    }

}

package ru.trippel.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    @NotNull
    List<Project> findAll();

    @Nullable
    List<Project> findAll(@Nullable String userId);

    @Nullable
    Project findOne(@Nullable String id);

    @Nullable
    Project persist(@Nullable Project project);

    @Nullable
    Project merge(@Nullable Project project);

    @Nullable
    Project remove(String id);

    void clear(@Nullable final String userId);

}

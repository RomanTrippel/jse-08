package ru.trippel.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.IProjectRepository;
import ru.trippel.tm.api.repository.ITaskRepository;
import ru.trippel.tm.api.service.IProjectService;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.repository.AbstractRepository;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository repository) {
        super(repository);
        projectRepository = repository;
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null) return null;
        if (userId.isEmpty()) return null;
        return repository.findAll(userId);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null) return;
        if (userId.isEmpty()) return;
        projectRepository.clear(userId);
    }

}

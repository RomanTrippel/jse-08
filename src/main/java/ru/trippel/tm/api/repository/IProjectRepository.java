package ru.trippel.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Project;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @NotNull
    List<Project> findAll();

    @Nullable
    List<Project> findAll(@NotNull String userId);

    @Nullable
    Project findOne(@NotNull String id);

    @Nullable
    Project persist(@NotNull Project project);

    @Nullable
    Project merge(@NotNull Project project);

    @Nullable
    Project remove(@NotNull String id);

    void removeAll();

    void clear(@NotNull String userId);

}

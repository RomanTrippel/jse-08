package ru.trippel.tm;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.util.PackageClassFinderUtil;

@NoArgsConstructor
public final class Application {

    @NotNull
    private static final Class<? extends AbstractCommand>[] CLASSES = PackageClassFinderUtil.getAllClasses("ru.trippel.tm.command");

    public static void main(String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(CLASSES);
    }

}
package ru.trippel.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Task;
import java.util.List;

public interface ITaskService extends IService<Task> {

    @NotNull
    List<Task> findAll();

    @Nullable
    List<Task> findAll(@Nullable String userId);

    @Nullable
    Task findOne(@Nullable String id);

    @Nullable
    Task persist(@Nullable Task task);

    @Nullable
    Task merge(@Nullable Task task);

    @Nullable
    Task remove(@Nullable String id);

    void removeAllByProjectId(@NotNull String projectId);

    void clear(@Nullable String userId);

}

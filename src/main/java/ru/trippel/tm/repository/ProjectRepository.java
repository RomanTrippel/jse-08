package ru.trippel.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.repository.IProjectRepository;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;

import java.util.*;

@NoArgsConstructor
public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        @NotNull final List<Project> listResult = new LinkedList<>();
        @NotNull final List<Project> listAll  = new LinkedList<>(map.values());
        for (@NotNull final Project project: listAll) {
            if (project.getUserId().equals(userId)) listResult.add(project);
        }
        return listResult;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<Project> listAll  = new LinkedList<>(map.values());
        for (@NotNull final Project project: listAll) {
            if (project.getUserId().equals(userId)) map.remove(project.getId());
        }
    }

}

package ru.trippel.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.context.IState;
import ru.trippel.tm.api.repository.IProjectRepository;
import ru.trippel.tm.api.repository.ITaskRepository;
import ru.trippel.tm.api.repository.IUserRepository;
import ru.trippel.tm.api.service.IProjectService;
import ru.trippel.tm.api.service.ITaskService;
import ru.trippel.tm.api.service.ITerminalService;
import ru.trippel.tm.api.service.IUserService;
import ru.trippel.tm.command.*;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.enumeration.TypeRole;
import ru.trippel.tm.repository.ProjectRepository;
import ru.trippel.tm.repository.TaskRepository;
import ru.trippel.tm.repository.UserRepository;
import ru.trippel.tm.service.TerminalService;
import ru.trippel.tm.service.ProjectService;
import ru.trippel.tm.service.TaskService;
import ru.trippel.tm.service.UserService;
import ru.trippel.tm.util.HashPasswordUtil;
import ru.trippel.tm.view.*;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@Getter
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator, IState {

    @NotNull
    private final Map<String, AbstractCommand> commands = new TreeMap<>();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final ITerminalService terminalService = new TerminalService();

    @NotNull
    private final IServiceLocator serviceLocator = this;

    @NotNull
    private final IState state = this;

    @Setter
    @Nullable
    private User currentUser = new User();

    @NotNull
    public final List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public void start(Class<? extends AbstractCommand>[] classes) throws Exception {
        BootstrapView.printWelcome();
        @NotNull String command = "";
        init(classes);
        while (true) {
            command = serviceLocator.getTerminalService().read();
            if ("EXIT".equals(command)) {
                BootstrapView.printGoodbye();
                break;
            }
            if (command.isEmpty()) {
                BootstrapView.printErrorCommand();
                continue;
            }
            execute(getCommand(command));
        }
    }

    private void execute(@Nullable final AbstractCommand command) {
        if (command == null) return;
        AbstractCommand commandTMP = checkPermission(command);
        if (commandTMP == null) return;
        try {
            commandTMP.execute();
        } catch (Exception e) {
            BootstrapView.printError();
        }
    }

    @Nullable
    private AbstractCommand getCommand(@Nullable final String command) {
        if (command == null) {
            return null;
        }
        if (command.isEmpty()) {
            return null;
        }
        @NotNull final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) BootstrapView.printErrorCommand();
        return abstractCommand;
    }

    @Nullable
    private AbstractCommand checkPermission(@Nullable final AbstractCommand command) {
        if (command == null || currentUser == null) return null;
        if (command.secure()) return command;
        String loginName = currentUser.getLoginName();
        if (loginName.isEmpty()) {
            BootstrapView.printErrorValidation();
            return null;
        }
        if (TypeRole.ADMIN == currentUser.getRole()) return command;
        if (currentUser.getRole() == command.getRole()) return command;
        return null;
    }

    public void init(Class<? extends AbstractCommand>[] classes) throws NoSuchAlgorithmException, InstantiationException,
            IllegalAccessException {
        register(classes);
        @NotNull final User admin = new User();
        admin.setLoginName("ADMIN");
        admin.setPassword(HashPasswordUtil.getHash("ADMIN"));
        admin.setRole(TypeRole.ADMIN);
        userService.persist(admin);
        @NotNull final User user = new User();
        user.setLoginName("USER");
        user.setPassword(HashPasswordUtil.getHash("USER"));
        userService.persist(user);
    }

    private void register(@NotNull final Class<? extends AbstractCommand>[] classes) throws IllegalAccessException,
            InstantiationException {
        @NotNull final int size = classes.length;
        if (size == 0) return;
        @NotNull Class<? extends AbstractCommand> classTemp;
        @NotNull AbstractCommand abstractCommand;
        for (int i = 0; i < size; i++) {
            classTemp = classes[i];
            if (classTemp.getSuperclass() == AbstractCommand.class){
                abstractCommand = classTemp.newInstance();
                abstractCommand.setServiceLocator(serviceLocator);
                abstractCommand.setState(state);
                commands.put(abstractCommand.getNameCommand(), abstractCommand);
            }
        }
    }

}

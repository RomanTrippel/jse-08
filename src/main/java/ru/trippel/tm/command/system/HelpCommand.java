package ru.trippel.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;

@NoArgsConstructor
public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "HELP";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "List commands.";
    }

    @Override
    public void execute() {
        System.out.println("Command list:");
        for (@NotNull final AbstractCommand command :
                state.getCommands()) {
            System.out.println(command.getNameCommand() + ": "
                    + command.getDescription());
        }
    }

}
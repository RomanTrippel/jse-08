package ru.trippel.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.service.TerminalService;
import java.util.List;

@NoArgsConstructor
public final class TaskEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "TASK_EDIT";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit a task.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User currentUser = state.getCurrentUser();
        @NotNull final String userId = currentUser.getId();
        @Nullable final List<Task> taskList = serviceLocator.getTaskService().findAll(userId);
        if (taskList == null) return;
        int taskNum = -1;
        System.out.println("Task List:");
        for (int i = 0; i < taskList.size(); i++) {
            System.out.println(i + 1 + ". " + taskList.get(i).getName());
        }
        System.out.println("Enter a task number to Edit:");
        taskNum +=  Integer.parseInt(serviceLocator.getTerminalService().read());
        @NotNull final String taskId = taskList.get(taskNum).getId();
        System.out.println("Enter new Name:");
        @NotNull final String newName = serviceLocator.getTerminalService().read();
        @Nullable final Task task = serviceLocator.getTaskService().findOne(taskId);
        if (task == null) return;
        task.setName(newName);
        System.out.println("Enter description:");
        @NotNull final String newDescription = serviceLocator.getTerminalService().read();
        task.setDescription(newDescription);
        serviceLocator.getTaskService().merge(task);
        System.out.println("Changes applied.");
    }

}

package ru.trippel.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.service.TerminalService;

@NoArgsConstructor
public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "TASK_CREATE";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Task task = new Task();
        @Nullable final User currentUser = state.getCurrentUser();
        @NotNull final String userId = currentUser.getId();
        System.out.println("Enter task name.");
        @NotNull final String name = serviceLocator.getTerminalService().read();
        task.setName(name);
        task.setUserId(userId);
        serviceLocator.getTaskService().persist(task);
        System.out.println("The task \"" + task.getName() + "\" added!");
    }

}

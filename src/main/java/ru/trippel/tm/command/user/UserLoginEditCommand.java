package ru.trippel.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.service.TerminalService;

@NoArgsConstructor
public final class UserLoginEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "USER_LOGINEDIT";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit a profile.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User currentUser = state.getCurrentUser();
        if (currentUser == null) {
            System.out.println("The current user is not defined.");
            return;
        }
        @NotNull final String userId = currentUser.getId();
        @Nullable final User user = serviceLocator.getUserService().findOne(userId);
        if (user == null) {
            System.out.println("Editable user is not defined.");
            return;
        }
        System.out.println("Enter new Login.");
        @NotNull final String newLogin = serviceLocator.getTerminalService().read();
        user.setLoginName(newLogin);
        serviceLocator.getUserService().merge(user);
        state.setCurrentUser(new User());
        System.out.println("Changes applied. Login required again.");
    }

}

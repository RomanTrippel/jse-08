package ru.trippel.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.entity.User;

import java.util.List;

@NoArgsConstructor
public final class TaskViewCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "TASK_VIEW";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        @Nullable final User currentUser = state.getCurrentUser();
        @NotNull final String userId = currentUser.getId();
        @Nullable final List<Task> taskList = serviceLocator.getTaskService().findAll(userId);
        if (taskList == null) return;
        System.out.println("Task List:");
        for (int i = 0; i < taskList.size(); i++) {
            System.out.println(i+1 + ". " + taskList.get(i));
        }
    }

}
